Rails.application.routes.draw do
  resources :reports
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  
  namespace :api do
    namespace :v1 do
      resources :users 
      post "login", to: "users#login"
      resources :reports
    end
  end
end
