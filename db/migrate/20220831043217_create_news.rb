class CreateNews < ActiveRecord::Migration[7.0]
  def change
    create_table :news do |t|
      t.string :name
      t.string :content
      t.date :date
      t.string :news_photo
      t.integer :type_id
      t.timestamps
    end
  end
end
