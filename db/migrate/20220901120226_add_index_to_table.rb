class AddIndexToTable < ActiveRecord::Migration[7.0]
  def change
    add_index :users, :role_id
    add_index :reports, :user_id
    add_index :news, :type_id
    add_index :roles, :id
    add_index :news_types, :id
  end
end
  