require 'rails_helper'

describe "Users API",type: :request do
  
  it "Return all users" do
    FactoryBot.create(:role, name: 'users')
    FactoryBot.create(:user, name: 'dhimas', email: 'dhimas@gmail.com', gender: 'laki-laki', ktp: '123', password_digest: '123', role_id: 1)
    FactoryBot.create(:user, name: 'sofia', email: 'sofia@gmail.com', gender: 'laki-laki', ktp: '123', password_digest: '123', role_id: 1)

    get '/api/v1/users'

    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(2)
    
  end

  it "creates a Widget" do
    
    post "/api/v1/users", :params => { :user => {:name => "dhim", :email => "dhim@gmail.com",:gender=>"laki", :ktp=> "123", :password_digest=> "123", :role_id => 1} }

    expect(response.content_type).to eq("application/json; charset=utf-8")
    expect(response).to have_http_status(:created)
  end
  
  
end
