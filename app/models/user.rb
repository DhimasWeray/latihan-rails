require 'bcrypt'

class User < ApplicationRecord
  belongs_to :role
  has_many :reports
  validates_uniqueness_of :email

  
  include BCrypt

  def password
    @password ||= Password.new(password_digest)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_digest = @password
  end
  
    def new_attribute 
      {
        id: self.id,
        email: self.email,
        name: self.name,
        role_name: self.role.name
      }
    end
end
