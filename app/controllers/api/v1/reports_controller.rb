module Api
  module V1
    class ReportsController < ApplicationController
      before_action :authorize, only: [:show, :update, :destroy]
      # GET /reports or /reports.json
      def index
        @reports = Report.all
        render json: @reports
      end

      def create
        @report = Report.new(report_params)
        if @report.save
          render json: @report, status: :ok
        else
          render json: {"message": @report.errors}, status: :unprocessable_entity
        end
      end

      def show
        @report = Report.where(:user_id => @user.id)
        render json: @report
      rescue ActiveRecord::RecordNotFound => e
        render json: {
          error: e.to_s
          }, status: :not_found
      end

      def update
        if @report.update(report_params)
          render json: @report
        else
          render json: @report.errors, status: :bad_request
        end
      end


      private
      
      def report_params
        params.require(:report).permit(:report_content, :date, :user_id)
      end

    end
  end
end
