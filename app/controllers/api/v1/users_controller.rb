module Api
  module V1
    class UsersController < ApplicationController
      before_action :authorize, only: [:show, :update, :destroy]
      def index
        @users = User.all
        render json: @users
      end

      def create
        @user = User.new(user_params)
        @user.password = user_params[:password_digest]
        token = encode_token({user_id: @user.id})
        if @user.save
          render json: {user: @user.new_attribute, token: token}
        else
          render json: {"message": @user.errors}, status: :bad_request
        end
      end

      def show
        @user = User.find_by_id(@user.id)
        render json: @user
      rescue ActiveRecord::RecordNotFound => e
        render json: {
          error: e.to_s
          }, status: :not_found
      end

      def update
        if @user.update(user_params)
          render json: @user.new_attribute
        else
          render json: @user.errors, status: :bad_request
        end
      end

      def destroy
        @user = User.find_by_id(params[:id])
        if @user.present?
          @user.destroy
          return render json: @user
        else
          return render json: {error: 'User not found'}
        end
      end

      def login
        @user = User.find_by_email(params[:email])

        if @user && @user.password == params[:password]
          token = encode_token({id: @user.id, email: @user.email})
          render json: {user: @user.new_attribute, token: token}
        else
          render json: {error: 'Invalid email or password'}, status: :unprocessable_entity
        end
      end
        

      private
      
      def user_params
        params.require(:user).permit(:name, :email, :gender, :ktp, :password_digest, :role_id)
      end
    end
  end
end


